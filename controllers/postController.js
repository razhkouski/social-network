import { Post } from "../models/Post.js";
import { User } from "../models/User.js";

export const createPost = async (req, res) => {
    const newPost = new Post(req.body);

    try {
        const savedPost = await newPost.save();

        res.status(200).json(savedPost);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const updatePost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        if (post.userId === req.body.userId) {
            await post.updateOne({ $set: req.body });

            res.status(200).json("Пост обновлён.");
        } else {
            res.status(403).json("Вы можете обновить только свой пост.");
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const deletePost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        if (post.userId === req.body.userId) {
            await post.deleteOne();

            res.status(200).json("Пост удалён.");
        } else {
            res.status(403).json("Вы можете удалить только свой пост.");
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const likeOrUnlikePost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        if (!post.likes.includes(req.body.userId)) {
            await post.updateOne({ $push: { likes: req.body.userId } });

            res.status(200).json("Пост лайкнут.");
        } else {
            await post.updateOne({ $pull: { likes: req.body.userId } });

            res.status(200).json("Пост анлайкнут.");
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const getPost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        res.status(200).json(post);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const getTimeline = async (req, res) => {
    try {
        const currentUser = await User.findById(req.body.userId);
        const userPosts = await Post.find({ userId: currentUser._id });
        const friendPosts = await Promise.all(
            currentUser.subscribers.map(friendId => Post.find({ userId: friendId }))
        );

        res.json(userPosts.concat(...friendPosts))
    } catch (e) {
        res.status(500).json(e);
    }
};