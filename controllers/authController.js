import jwt from 'jsonwebtoken';
import { User } from '../models/User.js';

const accessTokenSecret = 'top!Secret.Token^In#Da*World';

export const registration = async (req, res) => {
    try {
        if (!/\S+@\S+\.\S+/.test(req.body.email) || !req.body.email.trim()) {
            res.status(400).send("Формат e-mail адреса должен быть example@xxx.xx");
        } else {
            const user = await User.findOne({ email: req.body.email });

            if (user) {
                res.status(400).send("Пользователь с данным e-mail'ом уже зарегестрирован.");
            } else {
                User.create({ username: req.body.username, email: req.body.email, password: req.body.password }, (err, doc) => {
                    if (err) return res.status(400).send(err);
                    
                    res.send(`Новый пользователь зарегистрирован ${doc}.`);
                });
            }
        }
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
        console.log(e.message);
    }
};

export const login = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email, password: req.body.password });
        if (user) {
            const accessToken = jwt.sign({ email: user.email, isAdmin: user.isAdmin, _id: user._id, username: user.username }, accessTokenSecret);
            
            res.json({ accessToken });
        } else {
            res.status(400).send("Пользователь с данной комбинацией e-mail и пароля не был найден.");
        }
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
        console.log(e.message);
    }
};

export const authJWT = (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, accessTokenSecret, (err, user) => {
                if (err) return res.status(400).send("Неверный токен.");
                req.user = user;

                next();
            });
        } else {
            res.status(400).send("Не проведена авторизация.");
        }
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
    }
};

export const checkRole = (req, res, next) => {
    try {
        if (!req.user.isAdmin) return res.status(400).send("Недостаточно прав.");

        next();
    } catch (e) {
        res.status(500).send('Неверные параметры запроса');
    }
};