import { User } from "../models/User.js";

export const createUser = (req, res) => {
    try {
        User.create({ username: req.body.username, email: req.body.email, password: req.body.password }, (err, doc) => {
            if (err) return res.status(400).send(err);
            res.send(`Пользователь создан ${doc}`);
        });
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
    }
};

export const deleteUser = (req, res) => {
    try {
        User.deleteOne({ email: req.body.email }, (err, res) => {
            if (err) return res.status(400).send(err);
            res.send(`Пользователь удален`);
        });
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
    }
};

export const updateUser = (req, res) => {
    try {
        if (req.user.isAdmin) {
            User.updateOne({ email: req.body.email }, { username: req.body.newName }, (err, res) => {
                if (err) return res.status(400).send(err);
                res.send(`Имя пользователя обновлено`);
            });
        } else {
            User.updateOne({ username: req.user.username }, { username: req.body.newName }, (err, res) => {
                if (err) return res.status(400).send(err);
                res.send(`Имя пользователя обновлено`);
            });
        }
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
    }
};

export const getUserByID = (req, res) => {
    try {
        User.findById({ _id: req.body.Id }, (err, res) => {
            if (err) return res.status(400).send(err);
            res.send(`Данные получены: ${doc}`);
        });
    } catch (e) {
        res.status(500).send("Неверные параметры запроса.");
    }
};

export const followUser = async (req, res) => {
    if (req.body.userId !== req.params.id) {
        try {
            const user = await User.findById(req.params.id);
            const currentUser = await User.findById(req.body.userId);

            if (!user.followers.includes(req.body.userId)) {
                await user.updateOne({ $push: { followers: req.body.userId } });
                await currentUser.updateOne({ $push: { followings: req.params.id } });

                res.status(200).json(`Вы подписались на ${user.username}.`);
            } else {
                res.status(403).json("Вы уже подписаны на этого пользователя.");
            }
        } catch (e) {
            res.status(500).json(e);
        }
    } else {
        res.status(403).json("Вы не можете подписаться на самого себя.");
    }
};

export const unfollowUser = async (req, res) => {
    if (req.body.userId !== req.params.id) {
        try {
            const user = await User.findById(req.params.id);
            const currentUser = await User.findById(req.body.userId);

            if (user.followers.includes(req.body.userId)) {
                await user.updateOne({ $pull: { followers: req.body.userId } });
                await currentUser.updateOne({ $pull: { followings: req.params.id } });

                res.status(200).json(`Вы отписались от ${user.username}`);
            } else {
                res.status(403).json("Вы не подписаны на этого пользователя.");
            }
        } catch (e) {
            res.status(500).json(e);
        }
    } else {
        res.status(403).json("Вы не можете отписаться на самого себя.");
    }
};