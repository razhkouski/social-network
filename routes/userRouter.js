import express from 'express';
import { createUser, deleteUser, updateUser, getUserByID, followUser, unfollowUser } from "../controllers/userController.js";
import { authJWT, checkRole } from '../controllers/authController.js';

const userRouter = express.Router();

userRouter.post("/create", authJWT, checkRole, createUser); // .???
userRouter.put("/update", authJWT, updateUser);
userRouter.delete("/delete", authJWT, checkRole, deleteUser);
userRouter.get("/getByID", authJWT, getUserByID);
userRouter.put("/:id/follow", authJWT, followUser);
userRouter.put("/:id/unfollow", authJWT, unfollowUser);

export default userRouter;