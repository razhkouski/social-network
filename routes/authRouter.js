import express from 'express';
import { registration, login } from "../controllers/authController.js";
import userRouter from "./userRouter.js";

const authRouter = express.Router();

authRouter.post('/registration', registration);
authRouter.post('/login', login, userRouter);

export default authRouter;