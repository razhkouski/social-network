import express from 'express';
import { createPost, updatePost, deletePost, likeOrUnlikePost, getPost, getTimeline } from "../controllers/postController.js";
import { authJWT, checkRole } from '../controllers/authController.js';

const postRouter = express.Router();

postRouter.post("/", authJWT, checkRole, createPost);
postRouter.put("/:id", authJWT, checkRole, updatePost);
postRouter.delete("/:id", authJWT, checkRole, deletePost);
postRouter.put("/:id/like", authJWT, checkRole, likeOrUnlikePost);
postRouter.get("/:id", authJWT, getPost);
postRouter.get("/timeline/all", authJWT, getTimeline);

export default postRouter;