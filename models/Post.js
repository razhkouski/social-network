import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const postSchema = new mongoose.Schema({ 
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User", autopopulate: true, },
    desc: { type: String, max: 500, },
    img: { type: String, },
    likes: { type: Array, default: [], },
}, { versionKey: false });

postSchema.plugin(autopopulate);
const Post = mongoose.model("Post", postSchema); 

export { Post, postSchema };