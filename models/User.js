import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
 
const userSchema = new mongoose.Schema({
    username: { type: String, require: true, min: 3, max: 20, unique: true },
    email: { type: String, require: true, max: 50, unique: true },
    password: { type: String, require: true, min: 8, max: 30 },
    profilePicture: { type: String, default: "" }, 
    subscribers: { type: Array, default: [] },
    followers: { type: Array, default: [] },
    isAdmin: { type: Boolean, default: false },
}, { versionKey: false });

userSchema.plugin(autopopulate);
const User = mongoose.model("User", userSchema); 

export { User, userSchema };