import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';

import { authRouter, userRouter, postRouter } from './routes/index.js';

const app = express();

//middleware
app.use(cors());
app.use(express.json());

app.use("/auth", authRouter);
app.use("/users", userRouter);
app.use("/posts", postRouter);

mongoose.connect("mongodb://localhost:27017/socialNetworkDB", { useUnifiedTopology: true, useNewUrlParser: true }, () => {
    () => console.log('Something broke!');

    app.listen(8800, () => console.log("Server is running."));
});